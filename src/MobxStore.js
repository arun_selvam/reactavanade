import {observable, computed, action, extendObservable, autorun} from 'mobx';

class MobxStore {
    constructor(){
        extendObservable(this,{
            pageNames :["Home", "Page1", "Page2"],
            stuff :"asd"
        })
    }
}

var store  = new MobxStore()
export default store

autorun(() => {
    console.log(store.pageNames)
    console.log(store.stuff)
})