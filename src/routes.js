import React from 'react';
import {Switch, Route, Router} from 'react-router-dom';
import App from './App';
import Page1 from './containers/Page1'
import Page2 from './containers/Page2'
import Home from './containers/Home'

const Routes = () => (
  <Switch>
    <Route path='/' exact component={Home}/>
    <Route path='/page1' title="page1" component={Page1}/>
    <Route path='/page2'title="page2" component={Page2}/>
  </Switch>
);

export default Routes
