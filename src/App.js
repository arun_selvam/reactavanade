import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import RaisedButton from 'material-ui/RaisedButton';
import DrawerMenu from './components/DrawerMenu'
import Routes from './routes'
import store from './MobxStore'

class App extends React.Component {
  constructor(){
    super()
    // init state
    // will be called only once
  } 

  componentWillMount(){
  console.log("triggerd into App")
  }
 
  componentWillUnmount(){
    console.log("triggerd exit from App")
    }

  render() {
    return (
      <div>
        {store.stuff}
        <DrawerMenu/>
        <Routes/>
      </div>
    );
  }
}

export default App;