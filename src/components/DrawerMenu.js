import React, {Component} from 'react';
/* import Foo from './Foo';
import Bar from './Bar'; */
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Paper from 'material-ui/Paper';
import Assessment from 'material-ui/svg-icons/action/assessment';
import GridOn from 'material-ui/svg-icons/image/grid-on';
import {Toolbar, ToolbarTitle} from 'material-ui/Toolbar'
import Page1 from '../containers/Page1'
import {Link} from 'react-router-dom';
import {observer} from 'mobx-react'
import store from '../MobxStore'
//import {Link} from 'react-router';

import '../App.css';

const paperStyle = {
    height: '85%',
    width: "85%",
    margin: '7%',
    textAlign: 'center',
    display: 'inline-block'
};

const menuRoutes = [
    {
        text: 'Home',
        icon: <Assessment/>,
        link: '/'
    },
    {
        text: 'Page1',
        icon: <Assessment/>,
        link: '/page1'
    }, {
        text: 'Page2',
        icon: <GridOn/>,
        link: '/page2'
    }
]

 class DrawerMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            "open": false,
            "show": null,
            selectedMenu: 0
        };
    }

    componentDidUpdate(e) {
       console.log(e)
      }

    handleToggle = () => this.setState({
        open: !this.state.open
    });

    handleMenuItem = (test) => {
        this.setState({selectedMenu: test})
        store.stuff = menuRoutes[this.state.selectedMenu].text
    }
    render() {
        let content = null;
        return (
            <div className="App">

                <AppBar
                    iconClassNameRight="muidocs-icon-navigation-expand-more"
                   title={menuRoutes[this.state.selectedMenu].text}
                    onLeftIconButtonClick={this.handleToggle}/>
                <Drawer
                    docked={false}
                    width={200}
                    open={this.state.open}
                    onRequestChange={(open) => this.setState({open})}>

                    <AppBar title="Chokester"/> {/*<MenuItem id="page1" onClick={() => {this.handleMenuItem(1) }}>page 1</MenuItem>
                    <MenuItem id="page1" containerElement={< Link to = 'page2' />}>page 2</MenuItem>*/}
                    {menuRoutes.map((menu, index) => <MenuItem
                        key={index}
                        primaryText={menu.text}
                        leftIcon={menu.icon}
                        onClick={() => {
                        this.handleMenuItem(index)
                    }}
                        containerElement={< Link to = {
                        menu.link
                    } params={{test:"hello"}} />}/>)}

                </Drawer>
            </div>
        );
    }
}
export default observer(DrawerMenu)